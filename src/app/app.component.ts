import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '@app/user/authentication.service';
import { User } from '@app/user/user';

@Component({ selector: 'pm-root', templateUrl: 'app.component.html' })
// import './_content/app.less';
export class AppComponent {
  currentUser: User;
  userVerified: boolean;
  loggedIn: boolean;
  @ViewChild('sidenav') sidenav: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(
      x => (this.currentUser = x)
    );
    this.authenticationService.currentUser.subscribe(x =>
      this.currentUser ? (this.loggedIn = true) : (this.loggedIn = false)
    );
    this.authenticationService.userVerified.subscribe(
      x => (this.userVerified = x)
    );
  }

  logout() {
    this.authenticationService.logout();
    this.sidenav.close();
    this.router.navigate(['/login']);
  }
}
