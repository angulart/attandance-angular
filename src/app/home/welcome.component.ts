import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/user/user';
import { UserService } from '@app/user/user.service';
import { AuthenticationService } from '@app/user/authentication.service';

import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  templateUrl: './welcome.component.html'
})
export class WelcomeComponent implements OnInit {
  currentUser: User;
  user = {id: 'hb-0000', firstName: 'test', lastName: 'last', tech: 'web',department: 'web',profilePic:'assets/images/noImage.png'};
  users = [];
  imageObject: Array<object> = [{
      image: 'assets/img/slider/1.jpeg',
      thumbImage: 'https://images.pexels.com/photos/573130/pexels-photo-573130.jpeg',
      alt: 'alt of image',
      title: 'title of image'
    }, {
      image: 'https://images.pexels.com/photos/573130/pexels-photo-573130.jpeg', // Support base64 image
      thumbImage: 'https://images.pexels.com/photos/573130/pexels-photo-573130.jpeg', // Support base64 image
      title: 'Image title', //Optional: You can use this key if want to show image with title
      alt: 'Image alt' //Optional: You can use this key if want to show image with alt
    }
  ];
  

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
    console.log(this.currentUser);
    if (this.currentUser && this.currentUser.token) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(this.currentUser.token);
      console.log(decodedToken, 'aaaaaaaaaaaa');
      this.user = decodedToken;
    }
  }

  ngOnInit() {
    // this.loadAllUsers();
  }

  deleteUser(id: number) {
    this.userService
      .delete(id)
      .pipe(first())
      .subscribe(() => this.loadAllUsers());
  }

  private loadAllUsers() {
    this.userService
      .getAll()
      .pipe(first())
      .subscribe(users => (this.users = users));
  }
}
