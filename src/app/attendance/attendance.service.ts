import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { Attendance } from './attendance';
import { Time } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  private attendanceSubject: BehaviorSubject<Attendance>;
  public currentAttendance: Observable<Attendance>;
  public currentMarkedIn: BehaviorSubject<boolean>;
  public markedIn: Observable<boolean>;
  public currentMarkedOut: BehaviorSubject<boolean>;
  public markedOut: Observable<boolean>;

  constructor(private http: HttpClient) { 
    this.attendanceSubject = new BehaviorSubject<Attendance>(
      JSON.parse(localStorage.getItem('currentAttendance'))
    );
    this.currentAttendance = this.attendanceSubject.asObservable();

    this.currentMarkedIn = new BehaviorSubject<boolean>(
      localStorage.getItem('markedIn') == 'true' ? true : false
    );
    this.markedIn = this.currentMarkedIn.asObservable();

    this.currentMarkedOut = new BehaviorSubject<boolean>(
      localStorage.getItem('markedOut') == 'true' ? true : false
    );
    this.markedOut = this.currentMarkedOut.asObservable();
  }


  public get currentAttendanceValue(): Attendance {
    return this.currentAttendance.value;
  }

  public get currentMarkedInValue(): boolean {
    return this.markedIn.value;
  }

  attendenceIn(userId: string,markedTime: Time) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/attemdance_in`, {
        userId: userId,
        markedTime: markedTime
      })
      .pipe(
        map(attendence => {
          if (attendance.success) {
            localStorage.setItem('currentAttendance', JSON.stringify(attendance.data));
            localStorage.setItem('currentMarkedIn', 'true');
            this.attendanceSubject.next(attandance.data);
            this.markedIn.next(true);
          }
          return attendance;
        })
      );
  }

  attendenceOut(userId: string,markedTime: Time) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/attemdance_in`, {
        userId: userId,
        markedTime: markedTime
      })
      .pipe(
        map(attendence => {
          if (attendance.success) {
            localStorage.setItem('currentAttendance', JSON.stringify(attendance.data));
            localStorage.setItem('currentMarkedIn', 'true');
            this.attendanceSubject.next(attandance.data);
            this.markedOut.next(true);
          }
          return attendance;
        })
      );
  }
}
