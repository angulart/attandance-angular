import { Time } from '@angular/common';
import { Break } from './break';

export class Attendance{
  employeeCode?: string;
  attendanceIn: Boolean;
  attendanceInTime: Time;
  attendanceOut: Boolean;
  attendanceOutTime: Time;
  break: Array< Break >= [];
}