import { Time } from '@angular/common';

export class Break{
  employeeCode?: string;
  breakIn: Boolean;
  breakInTime: Time;
  breakOut: Boolean;
  breakOutTime: Time;
}