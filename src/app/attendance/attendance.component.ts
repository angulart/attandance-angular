import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/user/authentication.service';
import { User } from '@app/user/user';
import { AlertService } from '@app/alert/alert.service';


@Component({
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  attendenceForm: FormGroup;
  breakForm: FormGroup;
  emergencyLeaveForm: FormGroup;
  loading = false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
  submitted = false;
  returnUrl: string;
  error = '';
  currentUser: User;
  userObj = {id: 'hb-0000', firstName: 'test', lastName: 'last', tech: 'web',department: 'web',profilePic:'assets/images/noImage.png'};
  todayDate = new Date();
  step = 0;
  atd_in: string;
  atd_out: string;
  break = [];
  daysArray = [0.5,1,1.5,2,2.5,3,3.5,4,4.5,5]

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
  ) {
    if (this.authenticationService.currentUserValue) {
      this.authenticationService.currentUser.subscribe(
        x => (this.currentUser = x)
      );

      console.log(this.authenticationService.userVerifiedValue);
      if (
        this.authenticationService.currentUserValue &&
        this.authenticationService.userVerifiedValue
      ) {
        // this.router.navigate(['/']);
      }
    } else {
      // this.router.navigate(['/']);
    }
  }


  ngOnInit() {
    this.attendenceForm = this.formBuilder.group({
      pic: [''],
      in: [''],
      out: ['']
    });

    this.breakForm = this.formBuilder.group({
      pic: [''],
      in: [''],
      out: ['']
    });

    this.emergencyLeaveForm = this.formBuilder.group({
      startDate: [''],
      days: [''],
      comment: ['']
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  OnSubmit(){
    
  }
}
