import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { User } from './user';
// import { UserVerified } from './user_verified';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public currentUserVerified: BehaviorSubject<boolean>;
  public userVerified: Observable<boolean>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
    this.currentUserVerified = new BehaviorSubject<boolean>(
      localStorage.getItem('userVerified') == 'true' ? true : false
    );
    this.userVerified = this.currentUserVerified.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get userVerifiedValue(): boolean {
    return this.currentUserVerified.value;
  }

  login(username: string, password: string, type) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/login`, {
        email: username,
        password,
        type
      })
      .pipe(
        map(user => {
          console.log(user, 'user');
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          console.log(user.data);
          console.log(!user.data.twoFactorEnabled);
          if (user.success && !user.data.twoFactorEnabled) {
            localStorage.setItem('currentUser', JSON.stringify(user.data));
            localStorage.setItem('userVerified', 'true');
            this.currentUserSubject.next(user.data);
            this.currentUserVerified.next(true);
          } else if (user.success && user.data.twoFactorEnabled) {
            localStorage.setItem('currentUser', JSON.stringify(user.data));
            localStorage.setItem('userVerified', 'false');
            this.currentUserSubject.next(user.data);
            this.currentUserVerified.next(false);
          }
          return user;
        })
      );
  }

  forgotPassword(email: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/forgot-password`, {
        email
      })
      .pipe(
        map(user => {
          return user;
        })
      );
  }

  changePassword(password: string, token: string, id: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/set-password`, {
        password,
        code: token,
        id
      })
      .pipe(
        map(user => {
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  verify(email: string, otp: string, type) {
    return this.http
      .post<any>(`${environment.apiUrl}/web/verify-otp`, {
        email,
        otp,
        type
      })
      .pipe(
        map(userVerified => {
          if (userVerified.success) {
            localStorage.setItem(
              'currentUser',
              JSON.stringify(userVerified.data)
            );
            localStorage.setItem('userVerified', 'true');
            this.currentUserSubject.next(userVerified.data);
            this.currentUserVerified.next(true);
          }
          // this.userVerified = this.userVerified ? true : false;
          return userVerified;
        })
      );
    localStorage.setItem('UserVerified', JSON.stringify(true));
  }
}
