import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from './../authentication.service';
import { UserService } from './../user.service';
import { AlertService } from '@app/alert/alert.service';
import { MustMatch } from '@app/_helpers/must_match.validator';

@Component({
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  usertype: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.usertype = 'HB';
    this.registerForm = this.formBuilder.group(
      {
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        passwordHash: ['', [Validators.required, Validators.minLength(6)]],
        passwordConfirmation: ['', [Validators.required]],
        factory_authentication: true,
        type: this.usertype
      },
      {
        validator: MustMatch('passwordHash', 'passwordConfirmation')
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();
    console.log(this.registerForm.value);

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.registerForm.patchValue({ type: this.usertype });
    this.loading = true;
    this.userService
      .register(this.registerForm.value)
      //   .pipe(first())
      .subscribe(
        (data: any) => {
          if (data.success) {
            this.alertService.success(
              'Registration successful, please find the employee code sent you in mail.',
              true
            );
            this.router.navigate(['/login']);
          } else {
            this.error = data.message;
            alert(this.error);
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }
}
