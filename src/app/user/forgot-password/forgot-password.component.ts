import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from './../authentication.service';
import { first } from 'rxjs/operators';
import { User } from '@app/user/user';
import { AlertService } from '@app/alert/alert.service';

@Component({
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  currentUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    if (this.authenticationService.currentUserValue) {
    }
  }

  ngOnInit(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() {
    return this.forgotPasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.alertService.clear();

    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.forgotPassword(this.f.email.value).subscribe(
      data => {
        if (data.success) {
          this.alertService.success(
            'Email has been sent to your registered email address to set password.',
            true
          );
          this.returnUrl = '/login';
          this.router.navigate([this.returnUrl]);
        } else {
          this.error = data.message;
          alert(this.error);
          this.loading = false;
        }
      },
      error => {
        this.error = error;
        alert(this.error);
        this.loading = false;
      }
    );
  }
}
