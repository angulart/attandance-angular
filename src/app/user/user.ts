export class User {
  id: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  userType: string;
  employeeCode?: string;
  token?: string;
  data?: any;
  type?: any;
}
