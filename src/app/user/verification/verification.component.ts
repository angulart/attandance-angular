import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from './../authentication.service';
import { first } from 'rxjs/operators';
import { User } from '@app/user/user';
import { AlertService } from '@app/alert/alert.service';
import { AlertComponent } from '@app/alert/alert.component';

@Component({
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {
  verificationForm: FormGroup;
  @ViewChild('ngOtpInput') ngOtpInputRef: any;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  currentUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    if (this.authenticationService.currentUserValue) {
      this.authenticationService.currentUser.subscribe(
        x => (this.currentUser = x)
      );

      console.log(this.authenticationService.userVerifiedValue);
      if (
        this.authenticationService.currentUserValue &&
        this.authenticationService.userVerifiedValue
      ) {
        this.router.navigate(['/']);
      }
    } else {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {}

  submitVerification(input): void {
    this.alertService.clear();
    this.authenticationService
      .verify(this.currentUser.employeeCode, input, this.currentUser.type)
      .pipe(first())
      .subscribe(
        data => {
          if (data.success) {
            this.alertService.success(data.message, true);
            this.returnUrl = '/';
            this.router.navigate([this.returnUrl]);
          } else {
            this.ngOtpInputRef.setValue('');
            console.log(data.message, 'aaaaaaaaaaa');
            this.error = data.message;
            alert(this.error);
            this.loading = false;
          }
        },
        error => {
          this.ngOtpInputRef.setValue('');
          this.error = error;
          alert(this.error);
          this.loading = false;
        }
      );
  }

  onOtpChange(event: any): void {
    if (event.length == 6) {
      this.submitVerification(event);
      this.ngOtpInputRef.setValue();
    }
  }

  login() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
