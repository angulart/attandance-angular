import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { AuthenticationService } from './../authentication.service';
import {
  NgbActiveModal,
  NgbModal,
  ModalDismissReasons
} from '@ng-bootstrap/ng-bootstrap';
import { PwaService } from '../../_helpers/pwa.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  closeResult: string;
  usertype: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private modalService: NgbModal,
    public Pwa: PwaService
  ) {
    // redirect to home if already logged in
    console.log(this.authenticationService.userVerifiedValue);
    if (
      this.authenticationService.currentUserValue &&
      this.authenticationService.userVerifiedValue
    ) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    (this.usertype = 'HB'),
      (this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        usertype: ['HB']
      }));

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
    console.log(this.closeResult);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService
      .login(this.f.username.value, this.f.password.value, this.usertype)
      .subscribe(
        data => {
          if (data.success) {
            if (data.data && data.data.twoFactorEnabled) {
              console.log(data.data.twoFactorEnabled);
              this.returnUrl = '/verification';
            }
            this.router.navigate([this.returnUrl]);
          } else {
            console.log(data.message, 'aaaaaaaaaaa');
            this.error = data.message;
            alert(this.error);
            this.loading = false;
          }
          //   this.router.navigate([this.returnUrl]);
        },
        error => {
          //   this.error = error;
          //   this.loading = false;
          console.log(error, 'bbbbbbbbbb');
          this.error = error;
          alert(this.error);
          this.loading = false;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  installPwa(): void {
    this.Pwa.promptEvent.prompt();
  }
}
