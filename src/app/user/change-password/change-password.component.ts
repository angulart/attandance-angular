import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from './../authentication.service';
import { first } from 'rxjs/operators';
import { User } from '@app/user/user';
import { AlertService } from '@app/alert/alert.service';
import { MustMatch } from '@app/_helpers/must_match.validator';

@Component({
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  id = '';
  code = '';
  error = '';
  currentUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    if (this.authenticationService.currentUserValue) {
      this.authenticationService.currentUser.subscribe(
        x => (this.currentUser = x)
      );
      this.router.navigate(['/']);
    }
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.code = params['token'];
    });
  }

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordConfirmation: ['', [Validators.required]]
      },
      {
        validator: MustMatch('password', 'passwordConfirmation')
      }
    );

    // get return url from route parameters or default to '/'
    this.returnUrl =
      this.route.snapshot.queryParams['returnUrl'] || '/verification';
  }

  get f() {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.changePasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService
      .changePassword(this.f.password.value, this.code, this.id)
      .subscribe(
        data => {
          if (data.success) {
            this.alertService.success(data.message, true);
            this.returnUrl = '/login';
            this.router.navigate([this.returnUrl]);
          } else {
            this.error = data.message;
            alert(this.error);
            this.loading = false;
          }
        },
        error => {
          this.error = error;
          alert(this.error);
          this.loading = false;
        }
      );
  }
}
